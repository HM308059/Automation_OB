package pages;

import cucumber.runtime.junit.Assertions;
import junit.framework.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class  SegmentsPage {
	protected WebDriver driver;

	
	
	private By segmentsAudiencesMenu=By.xpath("(//span[contains(text(),'Audiences')])[1]");
	private By segmentsMenu=By.xpath("(//span[contains(text(),'Segments')])[1]");
	private By segmentsCreate=By.name("CreateNewInstanceButton_pyLanding_1");
	private By segmentsCreateOpen=By.name("//div[contains(text(),'Create and open')]");
	WebDriverWait w1;
	public SegmentsPage(WebDriver driver) {
	
		this.driver = driver;
		System.out.println("Title is "+driver.getTitle());
		if(!driver.getTitle().equals("Pega Marketing")) {
			
			throw new IllegalStateException("This is not Pega Marketing Campaign Page. The Current page is "
			+driver.getCurrentUrl());
		}
	}
	
	public void navigateSegments()
	{
		WebElement segmentsAudiences=driver.findElement(segmentsAudiencesMenu);
		Assert.assertEquals(true,segmentsAudiences.isDisplayed());
		segmentsAudiences.click();
		WebElement segments=driver.findElement(segmentsMenu);
		Assert.assertEquals(true,segments.isDisplayed());
		segments.click();
	}
	
	
	public void createSegments() throws Exception {
		w1=new WebDriverWait(driver,80);
		driver.switchTo().frame("PegaGadget2Ifr");
		w1.until(ExpectedConditions.presenceOfElementLocated(segmentsCreate));
		
		System.out.println("Create Button dislapyed "+driver.findElement(segmentsCreate).isDisplayed());
		System.out.println("Create Button Name dislapyed as "+driver.findElement(segmentsCreate).getText());
		driver.findElement(segmentsCreate).click();
		
		String filePath="C:/Users/r.b.ramamurthy/git/repository2/TMO_AutomationArtifact/Snapshot/Segmenst/SegmentsCreate-"+new SimpleDateFormat("dd-MM-yy-hh-mm-ss-ms").format(new Date())+".png";
		ScreenahotTaking.takeSnapShot(driver, filePath);
		Thread.sleep(15000);
	}
	
	public void createOpenSegments() throws Exception {
		w1=new WebDriverWait(driver,80);
		String filePath="C:/Users/r.b.ramamurthy/git/repository2/TMO_AutomationArtifact/Snapshot/Segments/SegmentsCreateOpen-"+new SimpleDateFormat("dd-MM-yy-hh-mm-ss-ms").format(new Date())+".png";
		ScreenahotTaking.takeSnapShot(driver, filePath);
		
//		w1.until(ExpectedConditions.presenceOfElementLocated(segmentsCreateOpen));
//		
//		System.out.println("Create Button dislapyed "+driver.findElement(segmentsCreateOpen).isDisplayed());
//		System.out.println("Create Button Name dislapyed as "+driver.findElement(segmentsCreateOpen).getText());
//		driver.findElement(segmentsCreate).click();
//		
//		String filePath1="C:/Users/r.b.ramamurthy/git/repository2/TMO_AutomationArtifact/Snapshot/Segments/SegmentsCreateOpen-"+new SimpleDateFormat("dd-MM-yy-hh-mm-ss-ms").format(new Date())+".png";
//		ScreenahotTaking.takeSnapShot(driver, filePath1);
	}
	
	
}
