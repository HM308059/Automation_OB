package pages;

import cucumber.runtime.junit.Assertions;
import junit.framework.Assert;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class  OffersPage {
	protected WebDriver driver;

	
	
	private By offersContentMenu=By.xpath("(//span[contains(text(),'Content')])[1]");
	private By offersMenu=By.xpath("(//span[contains(text(),'Offers')])[1]");
	private By offersCreate=By.name("CreateNewInstanceButton_pyLanding_3");
	private By offersCreateOpen=By.name("//div[contains(text(),'Create and open')]");
	WebDriverWait w1;
	public OffersPage(WebDriver driver) {
	
		this.driver = driver;
		System.out.println("Title is "+driver.getTitle());
		if(!driver.getTitle().equals("Pega Marketing")) {
			
			throw new IllegalStateException("This is not Pega Marketing Campaign Page. The Current page is "
			+driver.getCurrentUrl());
		}
	}
	
	public void navigateOffers()
	{
		WebElement offersContent=driver.findElement(offersContentMenu);
		Assert.assertEquals(true,offersContent.isDisplayed());
		offersContent.click();
		WebElement offers=driver.findElement(offersMenu);
		Assert.assertEquals(true,offers.isDisplayed());
		offers.click();
	}
	
	
	public void createOffers() throws Exception {

		w1=new WebDriverWait(driver,30);
		Thread.sleep(10000);
		driver.switchTo().frame("PegaGadget2Ifr");
		w1.until(ExpectedConditions.presenceOfElementLocated(offersCreate));
		System.out.println("Create Button dislapyed "+driver.findElement(offersCreate).isDisplayed());
		System.out.println("Create Button Name dislapyed as "+driver.findElement(offersCreate).getText());
		driver.findElement(offersCreate).click();
		
		
		String filePath="C:/Users/r.b.ramamurthy/git/repository2/TMO_AutomationArtifact/Snapshot/Offers/OffersCreate-"+new SimpleDateFormat("dd-MM-yy-hh-mm-ss-ms").format(new Date())+".png";
		ScreenahotTaking.takeSnapShot(driver, filePath);
		
	}
	
	public void createOpenOffers() throws Exception {

		w1=new WebDriverWait(driver,30);
		Thread.sleep(10000);
		
		
		System.out.println("Create Open Button dislapyed "+driver.findElement(offersCreateOpen).isDisplayed());
		System.out.println("Create Open Button Name dislapyed as "+driver.findElement(offersCreateOpen).getText());
		driver.findElement(offersCreateOpen).click();
		
		String filePath="C:/Users/r.b.ramamurthy/git/repository2/TMO_AutomationArtifact/Snapshot/Offers/OffersCreateOpen-"+new SimpleDateFormat("dd-MM-yy-hh-mm-ss-ms").format(new Date())+".png";
		ScreenahotTaking.takeSnapShot(driver, filePath);
	}
	
	
}
