package StepDefinitions;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.CampaignsPage;
import pages.LoginPage;
import pages.OffersPage;
import pages.SegmentsPage;


public class SegmentsPageStep {

	SegmentsPage sp;
	static	WebDriver  driver;

	@Then("user navigate to segments page")
	public void user_navigate_to_segments_page() {
	    // Write code here that turns the phrase above into concrete actions
		sp=new SegmentsPage(LoginPageStep.driver);

		System.out.println("User Navigate to segments Page");
		sp.navigateSegments();;
	    
	}
	
	@Then("user create and create open segments")
	public void user_create_and_create_open_segments() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		sp=new SegmentsPage(LoginPageStep.driver);

		System.out.println("User create and create open segments");
		sp.createSegments();
		sp.createOpenSegments();
	    
	}
	
	}




