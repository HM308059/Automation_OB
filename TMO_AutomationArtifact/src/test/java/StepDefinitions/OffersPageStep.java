package StepDefinitions;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.CampaignsPage;
import pages.LoginPage;
import pages.OffersPage;


public class OffersPageStep {

	OffersPage op;
	static	WebDriver  driver;

	@Then("user navigate to offer page")
	public void user_navigate_to_offer_page() {
	    // Write code here that turns the phrase above into concrete actions
		op=new OffersPage(LoginPageStep.driver);

		System.out.println("User Navigate to Multi Channel Campaign Page");
		op.navigateOffers();
	    
	}
	
	@Then("user create and create open offer")
	public void user_create_and_create_open_offer() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
		op=new OffersPage(LoginPageStep.driver);

		System.out.println("User create and create open offer");
		op.createOffers();
		op.createOpenOffers();
	    
	}
	
	}




